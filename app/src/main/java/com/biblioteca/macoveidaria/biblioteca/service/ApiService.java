//package com.biblioteca.macoveidaria.biblioteca.service;
//
//import com.biblioteca.macoveidaria.biblioteca.model.Carte;
//import com.biblioteca.macoveidaria.biblioteca.model.ResponseFromGetBooks;
//import com.biblioteca.macoveidaria.biblioteca.model.Token;
//import com.biblioteca.macoveidaria.biblioteca.model.User;
//import com.squareup.okhttp.ResponseBody;
//
//import java.util.List;
//
//import retrofit.http.Body;
//import retrofit.http.DELETE;
//import retrofit.http.GET;
//import retrofit.http.Header;
//import retrofit.http.POST;
//import retrofit.http.PUT;
//import retrofit.http.Path;
//import rx.*;
//import rx.Observable;
//
///**
// * Created by Macovei Daria on 11.01.2018.
// */
//
//public interface ApiService {
//    @GET("/users")
//    Observable<List<User>> getUsers();
//
//    @GET("/user/{userid}")
//    Observable<List<User>> getUserById(@Path("userid") int id);
//
//    @POST("/users/login")
//    Observable<Token> login(@Body User user);
//
//
//    //done
//    @GET("/books")
//    Observable<ResponseFromGetBooks> getBooks(@Header("x-auth-token") String authToken);
//
//    @GET("/books/{bookId}")
//    Observable<Carte> getBookById(@Path("bookId") int id);
//
//    //done
//    @POST("/books")
//    Observable<Carte> addBook(@Header("x-auth-token") String authToken, @Body Carte carte);
//
//    //done
//    @PUT("/books/{bookId}")
//    Observable<Carte> editBook(@Header("x-auth-token") String authToken, @Path("bookId") int id, @Body Carte carte);
//
//    //done
//    @DELETE("/books/{bookId}")
//    Observable<ResponseBody> delete(@Header("x-auth-token") String authToken, @Path("bookId") int id);
//}
