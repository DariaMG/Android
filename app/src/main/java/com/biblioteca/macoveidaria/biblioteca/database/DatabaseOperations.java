package com.biblioteca.macoveidaria.biblioteca.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.biblioteca.macoveidaria.biblioteca.Carte;
import com.biblioteca.macoveidaria.biblioteca.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Macovei Daria on 07.01.2018.
 */

public class DatabaseOperations extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION  = 1;
    private static final String DATABASE_NAME = "Biblioteca.db";
    private static final String DATABASE_LOCATION= "/data/data/com.biblioteca.macoveidaria.biblioteca/database/";

    private SQLiteDatabase myDataBase;

    private static final String TABLE_USERS = "User";
    private static final String TABLE_BIBLIOTECA = "Biblioteca";

    private static final String KEY_ID = "id";
    private static final String KEY_USER_NAME = "username";
    private static final String KEY_PASSWORD = "password";

    private static final String KEY_ID_B = "id";
    private static final String KEY_NUME = "nume";
    private static final String KEY_AUTOR = "autor";
    private static final String KEY_VERSIUNE = "versiune";

    private Context context;


    public String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + " ("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,, " + KEY_USER_NAME + " TEXT, "
            + KEY_PASSWORD + " TEXT" + ")";

    public String CREATE_BIBLIOTECA_TABLE = "CREATE TABLE " + TABLE_BIBLIOTECA + " ("
            + KEY_ID_B + " INTEGER PRIMARY KEY AUTOINCREMENT,, " + KEY_NUME + " TEXT, "
            + KEY_AUTOR + " TEXT, " +KEY_VERSIUNE + " INTEGER" + " ) ";

    public DatabaseOperations(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        Log.d("Database operations", "Database created");
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("Database operations", "UsersTable creatinga");
        sqLiteDatabase.execSQL(CREATE_USERS_TABLE);
        Log.d("Database operations", "UsersTable created");

        sqLiteDatabase.execSQL(CREATE_BIBLIOTECA_TABLE);
        Log.d("Database operations", "BibliotecaTable created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int oldVersion, int newVersion) {
    sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
    sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BIBLIOTECA);

        onCreate(sqLiteDatabase);

    }

    public void openDatabase() {
        Log.d("Database operations", "Database opened");
        String dbPath = context.getDatabasePath(DATABASE_NAME).getPath();
        if(myDataBase != null && myDataBase.isOpen()) {
            return;
        }
        myDataBase = SQLiteDatabase.openDatabase(dbPath,null,SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if(myDataBase != null) {
            myDataBase.close();
        }
    }

    public User getUser(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_USERS + " WHERE username = '" + username + "' and password = '" + password + "'", null);
        if (c.moveToFirst()) {
            do {

                int id = Integer.parseInt(c.getString(0));
                User user = new User(id, username, password);
                return user;
            } while (c.moveToNext());
        }
        return null;
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_NAME, user.getUsername());
        values.put(KEY_PASSWORD, user.getPassword());

        // Inserting Row
        long k = db.insert(TABLE_USERS, null, values);
        Log.d("Database operations", "UsersTable: one row inserted");
        //db.close(); // Closing database connection
    }

    public List<Carte> getAllBooks() {
        openDatabase();
        List<Carte> carti = new ArrayList<Carte>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BIBLIOTECA;

        Cursor cursor = myDataBase.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Carte carte = new Carte();
                carte.setId(Integer.parseInt(cursor.getString(0)));
                carte.setNume(cursor.getString(1));
                carte.setAutor(cursor.getString(2));
                // carte.setVersiune(Integer.parseInt(cursor.getString(3)));
                carti.add(carte);
            } while (cursor.moveToNext());
        }
        return carti;
    }

    public List<Carte> getBooksStartWith(String s) {
        List<Carte> carti = new ArrayList<Carte>();

        String selectQuery = "SELECT  * FROM " + TABLE_BIBLIOTECA + "WHERE "+KEY_NUME+" LIKE 'M%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Carte carte = new Carte();
                carte.setId(Integer.parseInt(cursor.getString(0)));
                carte.setNume(cursor.getString(1));
                carte.setAutor(cursor.getString(2));
                carti.add(carte);
            } while (cursor.moveToNext());
        }
        return carti;
    }

    public void deleteBook(Carte book) {
        openDatabase();
        SQLiteDatabase db = this.getWritableDatabase();

        // Delete Row
        db.delete(TABLE_BIBLIOTECA,"id=?",new String[]{String.valueOf(book.getId())});
        Log.d("Database operations", "UsersTable: one row inserted");
        //  db.delete(TABLE_CONTACTS, KEY_NAME + "=" + mname, null);
        // db.execSQL("DELETE FROM " + TABLE_NAME+ " WHERE "+COlUMN_NAME+"='"+value+"'");

    }

    public void addBook(Carte book) {
        openDatabase();
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NUME, book.getNume());
        values.put(KEY_AUTOR, book.getAutor());
        values.put(KEY_VERSIUNE, book.getAutor());

        // Inserting Row
        long k = db.insert(TABLE_BIBLIOTECA, null, values);
        Log.d("Database operations", "BibliotecaTable: one row inserted");
    }

    public void updateBook(Carte book) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NUME, book.getNume());
        values.put(KEY_AUTOR, book.getAutor());
        values.put(KEY_VERSIUNE, book.getAutor());

        // Update Row
        long k = db.update(TABLE_BIBLIOTECA,values,"id=?",new String[]{String.valueOf(book.getId())});
        Log.d("Database operations", "BibliotecaTable: one row inserted");

//        String strSQL = "UPDATE myTable SET Column1 = someValue WHERE columnId = "+ someValue;
//        db.execSQL(strSQL);
    }

    public void copyDataBase() throws IOException {
        String package_name = context.getPackageName();
        String DB_PATH = "/data/user/0/" + package_name + "/databases/";
        String DB_NAME = "CandyShop.db";
        try {
            InputStream myInput = context.getAssets().open(DB_NAME);

            File dbFile = new File(DB_PATH);
            dbFile.mkdirs();

            String outputFileName = DB_PATH + DB_NAME;
            OutputStream myOutput = new FileOutputStream(outputFileName);

            byte[] buffer = new byte[1024];
            int length;

            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
