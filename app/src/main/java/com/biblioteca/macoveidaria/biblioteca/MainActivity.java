package com.biblioteca.macoveidaria.biblioteca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.biblioteca.macoveidaria.biblioteca.adapter.ListProductAdapter;
import com.biblioteca.macoveidaria.biblioteca.database.DatabaseOperations;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button chartButton;
    List<Carte> carti;
    DatabaseOperations db;
    Button mailButton;
    Button addButton;
    ListView cartiListView;
    ListProductAdapter booksAdaptor;
    Carte carteToAdd;

    private static final int DETAILS_ACTIVITY_RESULT_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        carti = new ArrayList<>();
        db = new DatabaseOperations(this);
        // db.addBook(new Carte("qqq","qqq"));
//        try {
//            db.copyDataBase();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        chartButton = (Button) findViewById(R.id.chartButton);
        mailButton = (Button) findViewById(R.id.emailButton);
        addButton = (Button) findViewById(R.id.addButton);

        addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,DetailsActivity.class);
                intent.putExtra("Message","Add");
                startActivityForResult(intent,10);

            }
        });
    }

    protected void onStart() {
        super.onStart();
        // Get the reference of list
        cartiListView = (ListView) findViewById(R.id.list);

        // db.addBook(new Carte(2,"cccaaa","cccaaa",1));
        carti = db.getAllBooks();

        booksAdaptor = new ListProductAdapter(this, carti);
        // Set The Adapter
        cartiListView.setAdapter(booksAdaptor);

        // register onClickListener to handle click events on each item
        cartiListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            // argument position gives the index of item which is clicked
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {

                Carte selectedBook = carti.get(position);
                showBookPage(selectedBook);
                // Toast.makeText(getApplicationContext(),"Item pos is" + position+" "+selectedBook.getNume(),Toast.LENGTH_SHORT).show();
            }
        });

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEmailPage();
            }
        });

        chartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChartPage();
            }
        });
    }
    void showEmailPage(){
        Intent intent = new Intent(MainActivity.this,EmailActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    void showChartPage(){
        Intent intent = new Intent(MainActivity.this,ChartActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    void showBookPage(Carte selectedBook){
        Intent intent = new Intent(MainActivity.this,DetailsActivity.class);
//        Log.d("show bookDetailsPage",selectedBook.getId()+"");
//        Log.d("show bookDetailsPage",selectedBook.getNume()+"");
//        Log.d("show bookDetailsPage",selectedBook.getAutor()+"");
//        Log.d("show bookDetailsPage",selectedBook.getVersiune()+"");
        intent.putExtra("Message","Update");
        intent.putExtra("Id",selectedBook.getId());
        intent.putExtra("Nume",selectedBook.getNume());
        intent.putExtra("Autor",selectedBook.getAutor());
        intent.putExtra("Versiune",selectedBook.getVersiune());
        startActivityForResult(intent,10);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Log.d("show requestCode---", requestCode+"");
        // check that it is the SecondActivity with an OK result
        if (requestCode == DETAILS_ACTIVITY_RESULT_CODE) {
            Log.d("show requestCode", requestCode+"");
            if (resultCode == RESULT_OK) {

                // get String data from Intent
                String message = intent.getStringExtra("Message");
                Log.d("show Message", message);

                if(message.equals("Delete")) {
                    int id = intent.getIntExtra("Id",0);
                    String nume = intent.getStringExtra("Nume");
                    String autor = intent.getStringExtra("Autor");
                    int versiune = intent.getIntExtra("Versiune",0);

                    Carte carte = new Carte(id,nume,autor,versiune);
                    db.deleteBook(carte);
                    Log.d("show bookDetailsPage",carte.getId()+"");
                    Log.d("show boaaaokDetailsPage",carte.getNume()+"");
                    Log.d("show bookDetailsPage",carte.getAutor()+"");
                    Log.d("show bookDetailsPage",carte.getVersiune()+"");
                    carti = db.getAllBooks();

                    booksAdaptor = new ListProductAdapter(this, carti);
                    // Set The Adapter
                    cartiListView.setAdapter(booksAdaptor);
                }
                else if(message.equals("Update")) {
                    int id = intent.getIntExtra("Id",0);
                    String nume = intent.getStringExtra("Nume");
                    String autor = intent.getStringExtra("Autor");
                    int versiune = intent.getIntExtra("Versiune",0)+1;


                    Carte carte = new Carte(id,nume,autor,versiune);
                    db.updateBook(carte);
                    carti = db.getAllBooks();

                    booksAdaptor = new ListProductAdapter(this, carti);
                    // Set The Adapter
                    cartiListView.setAdapter(booksAdaptor);
                }

                else if(message.equals("Add")) {
                    String nume = intent.getStringExtra("Nume");
                    String autor = intent.getStringExtra("Autor");

                    carteToAdd = new Carte(nume,autor);
                    db.addBook(carteToAdd);
                    carti = db.getAllBooks();

                    booksAdaptor = new ListProductAdapter(this, carti);
                    // Set The Adapter
                    cartiListView.setAdapter(booksAdaptor);

                }

                else {
                    Log.d("show bookDetailsPage", "EEEERRRROOORRRMMMESSSAAGGE");
                }


            }
        }
    }

}
