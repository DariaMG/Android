package com.biblioteca.macoveidaria.biblioteca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DetailsActivity extends AppCompatActivity {
    private Carte carte;
    private String newMessage;
    private EditText numeEditText;
    private EditText autorEditText;
    private Button editBtn;
    private Button deleteBtn;
    private Button addBtn;
    private Carte newCarte;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        getSupportActionBar().setHomeButtonEnabled(true);

        editBtn = (Button) findViewById(R.id.updateCarte);
        deleteBtn = (Button) findViewById(R.id.deleteCarte);
        addBtn = (Button) findViewById(R.id.addCarte);

        Intent intent = getIntent();

        message = intent.getStringExtra("Message");
        Log.d("DetailsPage  message",message+"");

        int id = intent.getIntExtra("Id",0);
        Log.d("DetailsPage  id",id+"");

        String nume = intent.getStringExtra("Nume");
        Log.d("DetailsPage  nume",nume+"");

        String autor = intent.getStringExtra("Autor");
        Log.d("DetailsPage  autor",autor+"");

        int versiune = intent.getIntExtra("Versiune",0);
        Log.d("DetailsPage versiune",versiune+"");

        carte = new Carte(id,nume,autor,versiune);

        numeEditText = (EditText)findViewById(R.id.numeD);
        autorEditText = (EditText)findViewById(R.id.autorD);

        if(message.equals("Update")) {
            // set editText view with stringaaaa
            Log.d("If   DetailsPage  nume",nume+"");
            Log.d("If   DetailsPage  autor",autor+"");
            numeEditText.setText(nume);
            autorEditText.setText(autor);
            addBtn.setVisibility(View.INVISIBLE);
            editBtn.setVisibility(View.VISIBLE);
            deleteBtn.setVisibility(View.VISIBLE);
        }

        if(message.equals("Add")) {
            addBtn.setVisibility(View.VISIBLE);
            editBtn.setVisibility(View.INVISIBLE);
            deleteBtn.setVisibility(View.INVISIBLE);
        }


     addBtn.setOnClickListener(new View.OnClickListener() {

         @Override
         public void onClick(View v) {
             newMessage = "Add";
             String nume = numeEditText.getText().toString();
             String autor = autorEditText.getText().toString();
             newCarte = new Carte(carte.getId(),nume,autor,carte.getVersiune());
             // add(c);
             finish();

         }
     });

     deleteBtn.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            newMessage = "Delete";
            newCarte = carte;
           // delete(carte);
            finish();

        }
    });

    editBtn.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            newMessage = "Update";
            String nume = numeEditText.getText().toString();
            String autor = autorEditText.getText().toString();
            newCarte = new Carte(carte.getId(),nume,autor,carte.getVersiune());
            // edit(c);
            finish();

        }
    });
    }


    public void add(Carte c){
        Intent intent = new Intent(DetailsActivity.this,MainActivity.class);

        intent.putExtra("Message","Add");
        intent.putExtra("Nume",c.getNume());
        intent.putExtra("Autor",c.getAutor());

        // startActivity(intent);

    }

    public void edit(Carte c){
        Intent intent = new Intent(DetailsActivity.this,MainActivity.class);
        intent.putExtra("Message","Update");
        intent.putExtra("Id",c.getId());
        intent.putExtra("Nume",c.getNume());
        intent.putExtra("Autor",c.getAutor());
        intent.putExtra("Versiune",c.getVersiune());
        // startActivity(intent);
    }

    public void delete(Carte c){
        Intent intent = new Intent(DetailsActivity.this,MainActivity.class);
        intent.putExtra("Message","Delete");
        intent.putExtra("Id",c.getId());
        intent.putExtra("Nume",c.getNume());
        intent.putExtra("Autor",c.getAutor());
        intent.putExtra("Versiune",c.getVersiune());
        // startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        Intent intent = new Intent(DetailsActivity.this,MainActivity.class);

        intent.putExtra("Message",newMessage);
        intent.putExtra("Id",newCarte.getId());
        intent.putExtra("Nume",newCarte.getNume());
        intent.putExtra("Autor",newCarte.getAutor());
        intent.putExtra("Versiune",newCarte.getVersiune());

        setResult(RESULT_OK, intent);
        super.finish();
    }

}
