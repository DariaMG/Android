package com.biblioteca.macoveidaria.biblioteca;

import java.util.Date;

/**
 * Created by Macovei Daria on 07.01.2018.
 */

public class Carte {

    private int id;
    private String nume;
    private String autor;
    private int versiune;

    public Carte(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getVersiune() {
        return versiune;
    }

    public void setVersiune(int versiune) {
        this.versiune = versiune;
    }


    public Carte(int id, String nume, String autor, int versiune) {
        this.id = id;
        this.nume = nume;
        this.autor = autor;
        this.versiune = versiune;
    }

    public Carte( String nume, String autor) {
        this.id = 1;
        this.nume = nume;
        this.autor = autor;
        this.versiune =1;
    }

}
