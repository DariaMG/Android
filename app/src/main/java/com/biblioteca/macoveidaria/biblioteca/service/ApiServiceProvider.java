//package com.biblioteca.macoveidaria.biblioteca.service;
//
//import android.util.Log;
//
//import com.squareup.okhttp.Interceptor;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.Request;
//import com.squareup.okhttp.Response;
//import com.squareup.okhttp.logging.HttpLoggingInterceptor;
//
//import java.io.IOException;
//
//import javax.inject.Inject;
//import javax.inject.Provider;
//
//import retrofit.GsonConverterFactory;
//import retrofit.Retrofit;
//import retrofit.RxJavaCallAdapterFactory;
//
///**
// * Created by Macovei Daria on 13.01.2018.
// */
//
//public class ApiServiceProvider implements Provider<ApiService> {
//
//    protected static HttpLoggingInterceptor logging;
//    private static OkHttpClient httpClient;
//    private static Retrofit.Builder builder;
//
//    private static final String BASE_URL = "http://192.168.0.100:3000/";
//
//    @Inject
//    public ApiServiceProvider(OkHttpClient httpClient) {
//
//        logging = new HttpLoggingInterceptor();
//        this.httpClient = httpClient;
//
//        builder = new Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
//    }
//
//    @Override
//    public ApiService get() {
//
//        Log.d(getClass().getSimpleName(), " get call");
//        httpClient.interceptors().add(new Interceptor() {
//            @Override
//            public Response intercept(Interceptor.Chain chain) throws IOException {
//                Request original = chain.request();
//
//                Request.Builder requestbuilder = original.newBuilder()
//                        .header("Accept", "application/json");
//
//                Request request = requestbuilder.build();
//                return chain.proceed(request);
//            }
//        });
//
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        httpClient.interceptors().add(logging);
//
//        Retrofit retrofit = builder.client(httpClient).build();
//        return retrofit.create(ApiService.class);
//    }
//}
