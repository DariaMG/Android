package com.biblioteca.macoveidaria.biblioteca.adapter;

import android.content.Context;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biblioteca.macoveidaria.biblioteca.Carte;
import com.biblioteca.macoveidaria.biblioteca.R;

import java.util.List;

/**
 * Created by Macovei Daria on 08.01.2018.
 */

public class ListProductAdapter extends BaseAdapter {

    private Context myContext;
    private List<Carte> list;

    public ListProductAdapter(Context myContext, List<Carte> list) {
        this.myContext = myContext;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }
    
    @Override
    public long getItemId(int i) {
        return list.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(myContext, R.layout.item_listview,null);
        TextView tvName =(TextView)v.findViewById(R.id.nume);
        TextView tvAutor =(TextView)v.findViewById(R.id.autor);
        tvName.setText(list.get(i).getNume());
        tvAutor.setText(list.get(i).getAutor());
        return v;
    }
}
