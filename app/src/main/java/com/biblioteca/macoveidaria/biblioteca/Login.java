package com.biblioteca.macoveidaria.biblioteca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.biblioteca.macoveidaria.biblioteca.database.DatabaseOperations;


public class Login extends AppCompatActivity {

    DatabaseOperations db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        db = new DatabaseOperations(this);

        final EditText etUsername = (EditText) findViewById(R.id.etUsername);
        final EditText etPassword = (EditText) findViewById(R.id.etPassword);
        final Button bLogin = (Button) findViewById(R.id.bLogin);
        final TextView tvError = (TextView) findViewById(R.id.tvError);

        bLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String username= etUsername.getText().toString();
                String password = etPassword.getText().toString();
                MockUsers mockUsers = new MockUsers();

                // db.addUser(new User("bbb","bbb"));
                User user = db.getUser(username,password);

                if(user != null) {
                    Intent mainIntent = new Intent(Login.this, MainActivity.class);
                    Login.this.startActivity(mainIntent);
                }
                else {
                    tvError.setText("Username or password incorrect");
                }




            }
        });

    }
}
