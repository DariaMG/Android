package com.biblioteca.macoveidaria.biblioteca;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Macovei Daria on 24.10.2017.
 */

public class MockUsers {

    User u1 = new User(1,"aaa","aaa");
    User u2 = new User(2,"bbb","bbb");
    User u3 = new User(3,"ccc","ccc");


    public List<User> getUsersList() {
        List<User> usersList = new ArrayList<>();
        usersList.add(u1);
        usersList.add(u2);
        usersList.add(u3);
        return usersList;
    }
}
