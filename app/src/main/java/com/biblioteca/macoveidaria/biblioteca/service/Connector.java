//package com.biblioteca.macoveidaria.biblioteca.service;
//
//import android.content.SharedPreferences;
//import android.util.Log;
//
//import com.biblioteca.macoveidaria.biblioteca.model.Carte;
//import com.biblioteca.macoveidaria.biblioteca.model.ResponseFromGetBooks;
//import com.biblioteca.macoveidaria.biblioteca.model.Token;
//import com.biblioteca.macoveidaria.biblioteca.model.User;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import com.squareup.okhttp.ResponseBody;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import rx.Observable;
//import rx.functions.Action1;
//
///**
// * Created by Macovei Daria on 13.01.2018.
// */
//
//public class Connector {
//    private SharedPreferences sharedPreferences;
//    private ApiService apiService;
//
//
//    @Inject
//    public Connector(SharedPreferences sharedPreferences, ApiService apiService){
//        Log.d("---------","in getBooksFromServer");
//        this.sharedPreferences = sharedPreferences;
//        this.apiService = apiService;
//
//    }
//
//    public void saveBooks(List<Carte> books){
//        Gson gson = new Gson();
//        String jsonText = gson.toJson(books);
//        sharedPreferences.edit().putString("LIST", jsonText).apply();
//    }
//
//    public List<Carte> getBooks(){
//        Gson gson = new Gson();
//        String jsonText = sharedPreferences.getString("LIST", null);
//        List<Carte> books = gson.fromJson(jsonText, new TypeToken<List<Carte>>() {
//        }.getType());
//        return books;
//    }
//
//    public void deleteCarteFromDB(int id) {
//        List<Carte> carti = getBooks();
//        for(Carte c : carti){
//            if(c.getId() == id){
//                carti.remove(c);
//            }
//        }
//        saveBooks(carti);
//    }
//
//    public Observable<ResponseBody> deleteBooksFromServer(int id) {
//        return apiService.delete(sharedPreferences.getString("TOKEN", ""), id);
//    }
//
//    public Observable<ResponseFromGetBooks> getBooksFromServer() {
//        Log.d("---------","in getBooksFromServer");
//        return apiService.getBooks(sharedPreferences.getString("TOKEN", ""));
//    }
//
//    public Observable<Carte> postBooksOnServer(Carte carte) {
//        return apiService.addBook(sharedPreferences.getString("TOKEN", ""), carte);
//    }
//
//    public Observable<Carte> editBookOnServer(Carte carte) {
//        return apiService.editBook(sharedPreferences.getString("TOKEN", ""), carte.getId(), carte);
//    }
//
////    public Candy getCandyById(int candyId) {
////        List<Candy> candys = getCandys();
////        for(Candy c : candys){
////            if(c.getId() == candyId){
////                return c;
////            }
////        }
////        return null;
////    }
//
//    public Observable<Token> login(User user) {
//        return apiService.login(user)
//                .doOnNext(new Action1<Token>() {
//                    @Override
//                    public void call(Token token) {
//                        sharedPreferences.edit().putString("TOKEN", token.getToken()).apply();
//                    }
//                });
//    }
//
//}
